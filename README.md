# Playsite API

This project is an API for management of Playsite. Creating playsites with 4 types of Toys, adding/removing kids to playsites/queues and getting report of utilization is possible.

## Run Setup
- To run project in local:
    - Import project to your IDE.
    - Open a terminal and type `mvn clean install`
    - After the successful build you are ready to run app.

## Testing the Playsite API
- To test api:
    - Go the `http://localhost:8080/swagger-ui/index.html` for reaching Swagger UI.
    - Here you can test the API.

## Logic of Playsite API
- Playsites are created with given amount of Toys.
- Four toy types are available to use.
- To add a Kid to an existing Playsite, firstly Kid needs to be created with Kid API.
- Any kid added to a playsite is assigned to an available toy which is in Playsite.
- Toy availability means empty places exists in a toy.
- If playsite has no any available toys, it means that playsite capacity is full and kid will be added to playsite's queue if that kid is queueable.
- Order in queue is hold with a DB column named `ORDER_IN_QUEUE`
- If playsite is full and kid is not queueable, kid will not included to playsite's queue.
- Utilization is available in Playsite API for each Playsite created.
- Utilization of a Playsite has total visitors and utilization of each toy.


        


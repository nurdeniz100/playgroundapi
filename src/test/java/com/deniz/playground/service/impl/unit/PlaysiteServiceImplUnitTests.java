package com.deniz.playground.service.impl.unit;

import com.deniz.playground.dto.api.PlaysiteAddKidRequest;
import com.deniz.playground.dto.api.PlaysiteAddKidResponse;
import com.deniz.playground.dto.api.PlaysiteCreateRequest;
import com.deniz.playground.exception.ServiceException;
import com.deniz.playground.model.Kid;
import com.deniz.playground.model.PlaygroundToy;
import com.deniz.playground.model.Playsite;
import com.deniz.playground.model.ToyType;
import com.deniz.playground.repository.PlaysiteRepository;
import com.deniz.playground.service.impl.KidServiceImpl;
import com.deniz.playground.service.impl.PlaysiteServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PlaysiteServiceImplUnitTests {
    @Mock
    private PlaysiteRepository repository;
    @Mock
    private KidServiceImpl kidServiceMock;
    @InjectMocks
    private PlaysiteServiceImpl playsiteService;

    @Test
    void WHEN_PLAYSITE_CREATE_THEN_RETURN_SUCCESSFULL_RESPONSE() {
        PlaysiteCreateRequest request = new PlaysiteCreateRequest();
        request.setBallPitAmount(1);
        request.setCarouselAmount(1);
        request.setDoubleSwingAmount(1);
        request.setSlideAmount(1);
        Playsite playsiteCreated = createPlaysite(request);
        when(repository.save(any())).thenReturn(playsiteCreated);

        var actual = playsiteService.create(request);
        assertEquals("Playsite created successfully!", actual.getStatus());
    }

    @Test
    void WHEN_PLAYSITE_CREATE_WITH_EXCEPTION_THEN_RETURN_ERROR_RESPONSE() {
        PlaysiteCreateRequest request = new PlaysiteCreateRequest();
        request.setBallPitAmount(1);
        request.setCarouselAmount(1);
        request.setDoubleSwingAmount(1);
        request.setSlideAmount(1);
        when(repository.save(any())).thenThrow(new RuntimeException("Some error happened!"));
        var actual = playsiteService.create(request);
        assertEquals("Some error happened!", actual.getStatus());
    }

    @Test
    void WHEN_PLAYSITE_ADD_KID_THEN_RETURN_SUCCESSFULL_RESPONSE() {
        PlaysiteCreateRequest createRequest = new PlaysiteCreateRequest();
        createRequest.setBallPitAmount(1);
        createRequest.setCarouselAmount(1);
        createRequest.setDoubleSwingAmount(1);
        createRequest.setSlideAmount(1);

        Playsite playsiteCreated = createPlaysite(createRequest);
        when(repository.findById(1L)).thenReturn(Optional.of(playsiteCreated));
        when(kidServiceMock.getById(1L)).thenReturn(new Kid());

        PlaysiteAddKidRequest request = new PlaysiteAddKidRequest();
        request.setPlaysiteId(1L);
        request.setKidId(1L);
        PlaysiteAddKidResponse playsiteAddKidResponse = playsiteService.addKid(request);

        assertEquals("Kid added to playsite", playsiteAddKidResponse.getStatus());
    }

    @Test
    void WHEN_PLAYSITE_ADD_KID_THEN_RETURN_KID_ADD_QUEUE_RESPONSE() {
        PlaysiteCreateRequest createRequest = new PlaysiteCreateRequest();
        createRequest.setBallPitAmount(0);
        createRequest.setCarouselAmount(0);
        createRequest.setDoubleSwingAmount(0);
        createRequest.setSlideAmount(1);

        Playsite playsiteCreated = createPlaysite(createRequest);
        when(repository.findById(1L)).thenReturn(Optional.of(playsiteCreated));
        Kid kid = new Kid();
        kid.setIsQueueable(true);
        when(kidServiceMock.getById(1L)).thenReturn(kid);
        playsiteCreated.setKidsInPlaysite(List.of(new Kid()));

        PlaysiteAddKidRequest request = new PlaysiteAddKidRequest();
        request.setPlaysiteId(1L);
        request.setKidId(1L);
        PlaysiteAddKidResponse playsiteAddKidResponse = playsiteService.addKid(request);

        assertEquals("Kid added to queue", playsiteAddKidResponse.getStatus());
    }

    @Test
    void WHEN_PLAYSITE_ADD_KID_THEN_RETURN_KID_IS_NOT_QUEUE_RESPONSE() {
        PlaysiteCreateRequest createRequest = new PlaysiteCreateRequest();
        createRequest.setBallPitAmount(0);
        createRequest.setCarouselAmount(0);
        createRequest.setDoubleSwingAmount(0);
        createRequest.setSlideAmount(1);

        Playsite playsiteCreated = createPlaysite(createRequest);
        when(repository.findById(1L)).thenReturn(Optional.of(playsiteCreated));
        Kid kid = new Kid();
        kid.setIsQueueable(false);
        when(kidServiceMock.getById(1L)).thenReturn(kid);
        playsiteCreated.setKidsInPlaysite(List.of(new Kid()));

        PlaysiteAddKidRequest request = new PlaysiteAddKidRequest();
        request.setPlaysiteId(1L);
        request.setKidId(1L);
        PlaysiteAddKidResponse playsiteAddKidResponse = playsiteService.addKid(request);

        assertEquals("Playsite is full and kid doesnt want to be queued!", playsiteAddKidResponse.getStatus());
    }

    @Test
    void WHEN_PLAYSITE_ADD_KID_WITH_EXCEPTION_THEN_RETURN_ERROR_RESPONSE() {
        PlaysiteAddKidRequest request = new PlaysiteAddKidRequest();
        request.setPlaysiteId(1L);
        request.setKidId(1L);
        when(repository.findById(1L)).thenThrow(new ServiceException("Playsite not found!"));
        var actual = playsiteService.addKid(request);
        assertEquals("Playsite not found!", actual.getStatus());
    }

    public Playsite createPlaysite(PlaysiteCreateRequest request) {
        Playsite playsite = new Playsite();
        playsite.setKidsInPlaysite(new ArrayList<>());
        playsite.setKidsInQueue(new ArrayList<>());
        playsite.setTotalVisitors(0);
        playsite.setPlaygroundToys(List.of(
                new PlaygroundToy(ToyType.BALL_PIT, ToyType.BALL_PIT.getCapacity(), request.getBallPitAmount()),
                new PlaygroundToy(ToyType.CAROUSEL, ToyType.CAROUSEL.getCapacity(), request.getCarouselAmount()),
                new PlaygroundToy(ToyType.SLIDE, ToyType.SLIDE.getCapacity(), request.getSlideAmount()),
                new PlaygroundToy(ToyType.DOUBLE_SWING, ToyType.DOUBLE_SWING.getCapacity(), request.getDoubleSwingAmount()))
        );
        playsite.setTotalCapacity(playsite.getPlaygroundToys().stream()
                .map(playgroundToy -> playgroundToy.getCapacity() * playgroundToy.getAmount())
                .reduce(Integer::sum).orElse(0));
        return playsite;
    }
}

package com.deniz.playground.service.impl.integration;

import com.deniz.playground.dto.api.PlaysiteCreateRequest;
import com.deniz.playground.model.Playsite;
import com.deniz.playground.repository.PlaysiteRepository;
import com.deniz.playground.service.PlaysiteService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
class PlaysiteServiceImpllIntegrationTests {
    @Autowired
    private PlaysiteService playsiteService;

    @Autowired
    private PlaysiteRepository repository;

    @Test
    @Rollback
    void WHEN_PLAYSITE_CREATE_THEN_FIND_CREATED_PLAYSITE() {
        PlaysiteCreateRequest request = new PlaysiteCreateRequest();
        request.setBallPitAmount(1);
        request.setCarouselAmount(1);
        request.setDoubleSwingAmount(1);
        request.setSlideAmount(1);

        var actual = playsiteService.create(request);
        Optional<Playsite> foundEntity = repository.findById(actual.getCreatedPlaysiteId());

        assertNotNull(foundEntity);
        assertEquals(actual.getCreatedPlaysiteId(), foundEntity.get().getId());
    }
}

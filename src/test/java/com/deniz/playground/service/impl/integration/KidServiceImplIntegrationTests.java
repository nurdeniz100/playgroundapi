package com.deniz.playground.service.impl.integration;

import com.deniz.playground.dto.api.KidCreateRequest;
import com.deniz.playground.model.Kid;
import com.deniz.playground.repository.KidRepository;
import com.deniz.playground.service.KidService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
class KidServiceImplIntegrationTests {
    @Autowired
    private KidService kidService;

    @Autowired
    private KidRepository repository;

    @Test
    @Rollback
    void WHEN_KID_CREATE_THEN_FIND_CREATED_KID() {
        KidCreateRequest request = new KidCreateRequest();
        request.setName("Deniz");
        request.setAge(5);
        request.setQueueable(true);

        var actual = kidService.create(request);
        Optional<Kid> foundEntity = repository.findById(actual.getCreatedKidTicketNumber());

        assertNotNull(foundEntity);
        assertEquals(actual.getCreatedKidTicketNumber(), foundEntity.get().getTicketNumber());
    }
}

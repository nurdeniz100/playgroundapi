package com.deniz.playground.service.impl.unit;

import com.deniz.playground.dto.api.KidCreateRequest;
import com.deniz.playground.model.Kid;
import com.deniz.playground.repository.KidRepository;
import com.deniz.playground.service.impl.KidServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class KidServiceImplUnitTests {
    @InjectMocks
    private KidServiceImpl kidService;

    @Mock
    private KidRepository repository;

    public Kid createKid(KidCreateRequest request) {
        Kid kid = new Kid();
        kid.setName(request.getName());
        kid.setAge(request.getAge());
        kid.setIsQueueable(request.isQueueable());
        return kid;
    }

    @Test
    void WHEN_KID_CREATE_THEN_RETURN_SUCCESSFULL_RESPONSE() {
        KidCreateRequest request = new KidCreateRequest();
        request.setName("Deniz");
        request.setAge(5);
        request.setQueueable(true);
        Kid kidCreated = createKid(request);
        when(repository.save(any())).thenReturn(kidCreated);

        var actual = kidService.create(request);
        assertEquals("Kid created successfully!", actual.getStatus());
    }

    @Test
    void WHEN_KID_CREATE_WITH_EXCEPTION_THEN_RETURN_ERROR_RESPONSE() {
        KidCreateRequest request = new KidCreateRequest();
        request.setName("deniz");
        request.setAge(12);
        request.setQueueable(true);
        when(repository.save(any())).thenThrow(new RuntimeException("Some error happened!"));

        var actual = kidService.create(request);
        assertEquals("Some error happened!", actual.getStatus());
    }
}

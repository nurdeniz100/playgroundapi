package com.deniz.playground.dto.api;

import jakarta.annotation.Nonnull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
public class PlaysiteUtilizationRequest {
    @Nonnull
    private Long playsiteId;
}

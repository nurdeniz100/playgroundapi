package com.deniz.playground.dto.api;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PlaysiteCreateResponse extends BaseResponse {
    private Long createdPlaysiteId;
}

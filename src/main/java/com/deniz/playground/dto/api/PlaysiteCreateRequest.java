package com.deniz.playground.dto.api;

import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
public class PlaysiteCreateRequest {
    @Min(value = 0)
    private int slideAmount;
    @Min(value = 0)
    private int carouselAmount;
    @Min(value = 0)
    private int ballPitAmount;
    @Min(value = 0)
    private int doubleSwingAmount;
}

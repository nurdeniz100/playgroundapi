package com.deniz.playground.dto.api;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class KidCreateResponse extends BaseResponse {
    private Long createdKidTicketNumber;
}

package com.deniz.playground.dto.api;

import jakarta.annotation.Nonnull;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
public class KidCreateRequest {
    @NotBlank
    private String name;

    @Min(value = 3)
    @Max(value = 10)
    private Integer age;

    @Nonnull
    private boolean isQueueable;
}

package com.deniz.playground.dto.api;

import com.deniz.playground.model.ToyType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
public class PlaysiteUtilizationResponse extends BaseResponse {
    private double playsiteUtilizationPercentage;
    private Map<ToyType, Double> playgroundToyUtilizationPercentages;
    private int totalVisitorCount;
}

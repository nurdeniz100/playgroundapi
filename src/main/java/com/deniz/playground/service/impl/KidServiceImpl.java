package com.deniz.playground.service.impl;

import com.deniz.playground.dto.api.KidCreateRequest;
import com.deniz.playground.dto.api.KidCreateResponse;
import com.deniz.playground.exception.ServiceException;
import com.deniz.playground.model.Kid;
import com.deniz.playground.repository.KidRepository;
import com.deniz.playground.service.KidService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class KidServiceImpl implements KidService {
    private final KidRepository repository;

    @Override
    public KidCreateResponse create(KidCreateRequest request) {
        KidCreateResponse response = new KidCreateResponse();
        try {
            Kid kid = new Kid();
            kid.setName(request.getName());
            kid.setAge(request.getAge());
            kid.setIsQueueable(request.isQueueable());
            Kid kidCreated = repository.save(kid);
            response.setCreatedKidTicketNumber(kidCreated.getTicketNumber());
            response.setStatus("Kid created successfully!");
        } catch (Exception e) {
            response.setStatus(e.getMessage());
            log.error(ExceptionUtils.getStackTrace(e));
        }
        return response;
    }

    @Override
    public Kid getById(Long id) {
        return repository.findById(id).orElseThrow(() -> new ServiceException("Kid not found!"));
    }

    @Transactional
    @Override
    public void resetOrderInQueue(Long id) {
        repository.resetOrderInQueue(id);
    }
}

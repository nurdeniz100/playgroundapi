package com.deniz.playground.service.impl;

import com.deniz.playground.dto.api.*;
import com.deniz.playground.exception.ServiceException;
import com.deniz.playground.model.Kid;
import com.deniz.playground.model.PlaygroundToy;
import com.deniz.playground.model.Playsite;
import com.deniz.playground.model.ToyType;
import com.deniz.playground.repository.PlaysiteRepository;
import com.deniz.playground.service.KidService;
import com.deniz.playground.service.PlaysiteService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class PlaysiteServiceImpl implements PlaysiteService {
    private final PlaysiteRepository repository;
    private final KidService kidService;

    @Override
    public PlaysiteCreateResponse create(PlaysiteCreateRequest request) {
        PlaysiteCreateResponse response = new PlaysiteCreateResponse();
        try {
            Playsite playsite = new Playsite();
            playsite.setKidsInPlaysite(new ArrayList<>());
            playsite.setKidsInQueue(new ArrayList<>());
            List<PlaygroundToy> toys = createToyListFromDto(request);
            toys.forEach(toy -> toy.setPlaysite(playsite));
            playsite.setPlaygroundToys(toys);
            Integer totalCapacity = toys.stream()
                    .map(playgroundToy -> playgroundToy.getCapacity() * playgroundToy.getAmount())
                    .reduce(Integer::sum).orElseThrow(RuntimeException::new);
            playsite.setTotalCapacity(totalCapacity);
            Playsite savedPlaySite = repository.save(playsite);
            response.setCreatedPlaysiteId(savedPlaySite.getId());
            response.setStatus("Playsite created successfully!");
        } catch (Exception e) {
            response.setStatus(e.getMessage());
            log.error(ExceptionUtils.getStackTrace(e));
        }
        return response;
    }

    @Override
    public PlaysiteAddKidResponse addKid(PlaysiteAddKidRequest request) {
        PlaysiteAddKidResponse response = new PlaysiteAddKidResponse();

        try {
            Playsite playsite = repository.findById(request.getPlaysiteId()).orElseThrow(() -> new ServiceException("Playsite not found!"));
            Kid kid = kidService.getById(request.getKidId());

            if (playsite.getTotalCapacity() > playsite.getKidsInPlaysite().size()) {
                playsite.getKidsInPlaysite().add(kid);
                playsite.setTotalVisitors(playsite.getTotalVisitors() + 1);
                playsite.getPlaygroundToys().stream()
                        .filter(playgroundToy -> playgroundToy.getKidCount() < playgroundToy.getCapacity() * playgroundToy.getAmount())
                        .findAny()
                        .ifPresent(PlaygroundToy::addKid);
                kid.setCurrentPlaysite(playsite);
                repository.save(playsite);
                response.setStatus("Kid added to playsite");
            } else if (kid.getIsQueueable()) {
                playsite.getKidsInQueue().add(kid);
                kid.setQueuePlaysite(playsite);
                repository.save(playsite);
                response.setStatus("Kid added to queue");
            } else {
                response.setStatus("Playsite is full and kid doesnt want to be queued!");
            }
        } catch (Exception e) {
            response.setStatus(e.getMessage());
            log.error(ExceptionUtils.getStackTrace(e));
        }
        return response;
    }

    @Override
    public PlaysiteRemoveKidResponse removeKid(PlaysiteRemoveKidRequest request) {
        PlaysiteRemoveKidResponse response = new PlaysiteRemoveKidResponse();

        try {
            Kid kid = kidService.getById(request.getKidId());

            if (kid.getCurrentPlaysite() == null && kid.getQueuePlaysite() == null) {
                response.setStatus("Kid is not in any playsite and is not in queue!");
                return response;
            }

            Optional.ofNullable(kid.getCurrentPlaysite()).ifPresent(playsite -> {
                kid.setCurrentPlaysite(null);
                playsite.getKidsInPlaysite().remove(kid);

                if (!playsite.getKidsInQueue().isEmpty()) {
                    Kid removedKidFromQueue = playsite.getKidsInQueue().remove(0);
                    playsite.getKidsInPlaysite().add(removedKidFromQueue);
                    removedKidFromQueue.setCurrentPlaysite(playsite);
                    removedKidFromQueue.setQueuePlaysite(null);
                    kidService.resetOrderInQueue(removedKidFromQueue.getTicketNumber());
                    playsite.setTotalVisitors(playsite.getTotalVisitors() + 1);
                } else {
                    playsite.getPlaygroundToys().stream()
                            .filter(playgroundToy -> playgroundToy.getKidCount() > 0)
                            .findAny()
                            .ifPresent(PlaygroundToy::removeKid);
                }

                repository.save(playsite);
                response.setStatus("Kid successfully removed from playsite!");
            });

            Optional.ofNullable(kid.getQueuePlaysite()).ifPresent(playsite -> {
                kid.setQueuePlaysite(null);
                playsite.getKidsInQueue().remove(kid);

                repository.save(playsite);
                response.setStatus("Kid successfully removed from queue!");
            });
        } catch (Exception e) {
            response.setStatus(e.getMessage());
            log.error(ExceptionUtils.getStackTrace(e));
        }
        return response;
    }

    @Override
    public PlaysiteUtilizationResponse getUtilization(Long playsiteId) {
        PlaysiteUtilizationResponse response = new PlaysiteUtilizationResponse();
        try {
            Playsite playsite = repository.findById(playsiteId).orElseThrow(() -> new ServiceException("Playsite not found!"));
            response.setTotalVisitorCount(playsite.getTotalVisitors());
            Map<ToyType, Double> toyUtilizations = playsite.getPlaygroundToys().stream().collect(Collectors.toMap(PlaygroundToy::getType, PlaygroundToy::countCapacity));
            response.setPlaygroundToyUtilizationPercentages(toyUtilizations);
            double totalUtilization = playsite.getPlaygroundToys().stream().map(PlaygroundToy::countCapacity).reduce(Double::sum).orElse((double) 0) / playsite.getPlaygroundToys().size();
            response.setPlaysiteUtilizationPercentage(totalUtilization);
            response.setStatus("Utilization is calculated!");
        } catch (Exception e) {
            response.setStatus(e.getMessage());
            log.error(ExceptionUtils.getStackTrace(e));
        }
        return response;
    }

    private List<PlaygroundToy> createToyListFromDto(PlaysiteCreateRequest request) {
        List<PlaygroundToy> playgroundToys = new ArrayList<>();
        if (request.getBallPitAmount() > 0) {
            playgroundToys.add(new PlaygroundToy(ToyType.BALL_PIT, ToyType.BALL_PIT.getCapacity(), request.getBallPitAmount()));
        }
        if (request.getCarouselAmount() > 0) {
            playgroundToys.add(new PlaygroundToy(ToyType.CAROUSEL, ToyType.CAROUSEL.getCapacity(), request.getCarouselAmount()));
        }
        if (request.getSlideAmount() > 0) {
            playgroundToys.add(new PlaygroundToy(ToyType.SLIDE, ToyType.SLIDE.getCapacity(), request.getSlideAmount()));
        }
        if (request.getDoubleSwingAmount() > 0) {
            playgroundToys.add(new PlaygroundToy(ToyType.DOUBLE_SWING, ToyType.DOUBLE_SWING.getCapacity(), request.getDoubleSwingAmount()));
        }
        return playgroundToys;
    }
}

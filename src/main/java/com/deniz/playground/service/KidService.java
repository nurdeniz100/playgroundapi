package com.deniz.playground.service;

import com.deniz.playground.dto.api.KidCreateRequest;
import com.deniz.playground.dto.api.KidCreateResponse;
import com.deniz.playground.model.Kid;

public interface KidService {
    KidCreateResponse create(KidCreateRequest request);

    Kid getById(Long id);

    void resetOrderInQueue(Long id);

}

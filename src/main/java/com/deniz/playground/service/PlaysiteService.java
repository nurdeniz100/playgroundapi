package com.deniz.playground.service;

import com.deniz.playground.dto.api.*;

public interface PlaysiteService {
    PlaysiteCreateResponse create(PlaysiteCreateRequest request);

    PlaysiteAddKidResponse addKid(PlaysiteAddKidRequest request);

    PlaysiteRemoveKidResponse removeKid(PlaysiteRemoveKidRequest request);

    PlaysiteUtilizationResponse getUtilization(Long playsiteId);
}

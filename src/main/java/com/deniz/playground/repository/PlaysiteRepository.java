package com.deniz.playground.repository;

import com.deniz.playground.model.Playsite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlaysiteRepository extends JpaRepository<Playsite, Long> {
}

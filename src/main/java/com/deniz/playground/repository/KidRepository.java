package com.deniz.playground.repository;

import com.deniz.playground.model.Kid;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface KidRepository extends JpaRepository<Kid, Long> {
    @Query(value = "UPDATE KID set ORDER_IN_QUEUE = NULL WHERE TICKET_NUMBER = :id", nativeQuery = true)
    @Modifying
    void resetOrderInQueue(Long id);
}

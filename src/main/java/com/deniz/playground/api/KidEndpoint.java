package com.deniz.playground.api;

import com.deniz.playground.dto.api.KidCreateRequest;
import com.deniz.playground.dto.api.KidCreateResponse;
import com.deniz.playground.service.KidService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/kid")
public class KidEndpoint {
    private final KidService kidService;

    @PostMapping("/create")
    public ResponseEntity<KidCreateResponse> create(@Valid @RequestBody KidCreateRequest request) {
        KidCreateResponse response = kidService.create(request);
        return ResponseEntity.ok(response);
    }
}

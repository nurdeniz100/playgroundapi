package com.deniz.playground.api;

import com.deniz.playground.dto.api.*;
import com.deniz.playground.service.PlaysiteService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/playsite")
public class PlaysiteEndpoint {
    private final PlaysiteService playsiteService;

    @PostMapping("/create")
    public ResponseEntity<PlaysiteCreateResponse> create(@Valid @RequestBody PlaysiteCreateRequest request) {
        PlaysiteCreateResponse response = playsiteService.create(request);
        return ResponseEntity.ok(response);
    }

    @PutMapping("/addKid")
    public ResponseEntity<PlaysiteAddKidResponse> addKid(@Valid @RequestBody PlaysiteAddKidRequest request) {
        PlaysiteAddKidResponse response = playsiteService.addKid(request);
        return ResponseEntity.ok(response);
    }

    @PutMapping("/removeKid")
    public ResponseEntity<PlaysiteRemoveKidResponse> removeKid(@Valid @RequestBody PlaysiteRemoveKidRequest request) {
        PlaysiteRemoveKidResponse response = playsiteService.removeKid(request);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/utilization")
    public ResponseEntity<PlaysiteUtilizationResponse> getUtilization(@Valid @RequestParam Long playsiteId) {
        PlaysiteUtilizationResponse response = playsiteService.getUtilization(playsiteId);
        return ResponseEntity.ok(response);
    }
}

package com.deniz.playground.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
public class Playsite {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "playsite")
    @Setter
    private List<PlaygroundToy> playgroundToys;

    @Setter
    private Integer totalCapacity;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "currentPlaysite")
    @Setter
    private List<Kid> kidsInPlaysite;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "queuePlaysite")
    @Setter
    @OrderColumn(name = "order_in_queue")
    private List<Kid> kidsInQueue;

    @Setter
    private Integer totalVisitors = 0;
}

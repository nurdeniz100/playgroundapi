package com.deniz.playground.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Entity
@RequiredArgsConstructor
public class PlaygroundToy {

    @Enumerated(value = EnumType.STRING)
    private final ToyType type;

    private final int capacity;

    private final int amount;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @Setter
    private Playsite playsite;

    @Setter
    private int kidCount = 0;

    protected PlaygroundToy() {
        this.capacity = 0;
        this.amount = 0;
        this.type = null;
    }

    public void addKid() {
        if (this.kidCount < capacity * amount) {
            this.kidCount++;
        }
    }

    public void removeKid() {
        if (this.kidCount > 0) {
            this.kidCount--;
        }
    }

    public double countCapacity() {
        return ((double) kidCount / (double) capacity * amount) * 100;
    }
}

package com.deniz.playground.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ToyType {
    BALL_PIT(5),
    CAROUSEL(8),
    DOUBLE_SWING(2),
    SLIDE(1);

    private final int capacity;
}

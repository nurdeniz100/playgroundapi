package com.deniz.playground.model;

import jakarta.annotation.Nonnull;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
public class Kid {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ticketNumber;

    @Nonnull
    @Setter
    private String name;

    @Nonnull
    @Setter
    private Integer age;

    @Nonnull
    @Setter
    private Boolean isQueueable;

    @ManyToOne(cascade = CascadeType.MERGE)
    @Setter
    private Playsite currentPlaysite;

    @ManyToOne(cascade = CascadeType.MERGE)
    @Setter
    private Playsite queuePlaysite;
}
